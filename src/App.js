import React, { Component } from 'react';
import './App.less';
import Table from './components/Table/Table'
import TableHead from './components/Table/TableHead/TableHead'
import TableBody from './components/Table/TableBody/TableBody'
import TableRow from './components/Table/TableRow/TableRow'
import TableCell from './components/Table/TableCell/TableCell'
import Sort from './components/UI/Sort/Sort'
import PopupUser from './components/UI/PopupUser/PopupUser'
import axios from 'axios'

class App extends Component {

  state = {
    contacts: [],
    userPopup: {
      show: false,  // Юзер в попапе
      idContact: 0,
      edit:false
    },
    userName: '',   // имя юзера дл поиска
    tableHead:  {
      list: [
        {
          id: 'name',
          name: 'Имя',
          sort: ''
        },
        {
          id: 'username',
          name: 'Ник',
          sort: ''
        },
        {
          id: 'email',
          name: 'Почта',
          sort: ''
        }
      ],
      lastSort: {
        index: null,
        sort: ''
      } // где последний раз была сортировка
    },
  };

  componentDidMount() {
    window.addEventListener("beforeunload", () =>
    {
      window.localStorage.setItem('contacts', JSON.stringify(this.state.contacts));
    });


    const loadContacts = JSON.parse(window.localStorage.getItem('contacts'));
    console.log(loadContacts)
    if (!loadContacts || loadContacts.length === 0){
      let newContacts = []
      axios
        .get("http://demo.sibers.com/users")
        .then(

          response => {
            newContacts = response.data.map(item => {
              let copy = {...item};
              copy.sortShow = true;           // скрывает сортировка
              return copy
            });
            this.setState({contacts: newContacts})
          }
        );
    }
    else if (loadContacts.length !== 0){
     this.setState({
       contacts:loadContacts
     })
    }


  }
  // Сортировка
  sortByNameTable = () => {
    const regex = new RegExp(this.state.userName);
    const oldPlayers = [...this.state.contacts]

    let sortContacts = oldPlayers.map((el,i) => {
      if (regex.test(el.name)){
        const newEl = {...el}
        newEl.sortShow = true;
        return (newEl)
      }
      else{
        const newEl = {...el}
        newEl.sortShow = false;
        return (newEl)
      }
    })

    this.setState({ contacts: sortContacts })
  }

  changeNameHandler = (event) => {
    this.setState({
      userName:  event.target.value
    }, () => {
      this.sortByNameTable();
    });
  }

  compareByAsc(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  compareByDesc(key) {
    return function (a, b) {
      if (a[key] > b[key]) return -1;
      if (a[key] < b[key]) return 1;
      return 0;
    };
  }

  sortBy(key,by) {
    let arrayCopy = [...this.state.contacts];
    if (by === "asc")
      arrayCopy.sort(this.compareByAsc(key));
    else
      arrayCopy.sort(this.compareByDesc(key));
    this.setState({contacts: arrayCopy});
  }

  sortHandler = (index, by) => {
    let oldTableHead =  {...this.state.tableHead};
    const key = oldTableHead.list[index].id;
    const lastSortIndex = oldTableHead.lastSort.index;

    if(lastSortIndex === null){
      oldTableHead.list[index].sort = by;
      oldTableHead.lastSort.index = index;
    }else {
      oldTableHead.list[lastSortIndex].sort = '';
      oldTableHead.list[index].sort = by;
      oldTableHead.lastSort.index = index;
    }

    this.setState({tableHead: oldTableHead})
    this.sortBy(key,by)
  }
  // Сортировка END

  closePopupUser = () => {
    let old = {...this.state.userPopup};
    old.show = false
    this.setState({userPopup:old})
  }

  editPopupUser = () =>{
    let old = {...this.state.userPopup};
    const updContacts = [...this.state.contacts]
    old.edit = !this.state.userPopup.edit

    if (old.edit){
      this._nameEl.classList.add("edit")
      this._nameEl.setAttribute("contenteditable", "true");

      this._emailEl.classList.add("edit")
      this._emailEl.setAttribute("contenteditable", "true");

      this._websiteEl.classList.add("edit")
      this._websiteEl.setAttribute("contenteditable", "true");

      this._phoneEl.classList.add("edit")
      this._phoneEl.setAttribute("contenteditable", "true");
    }else{

      this._nameEl.classList.remove("edit")
      this._nameEl.setAttribute("contenteditable", "false");

      this._emailEl.classList.remove("edit")
      this._emailEl.setAttribute("contenteditable", "false");

      this._websiteEl.classList.remove("edit")
      this._websiteEl.setAttribute("contenteditable", "false");

      this._phoneEl.classList.remove("edit")
      this._phoneEl.setAttribute("contenteditable", "false");


      const editContact = {...updContacts[this.state.userPopup.idContact]}

      editContact.name =    this._nameEl.innerText;
      editContact.email =   this._emailEl.innerText;
      editContact.website = this._websiteEl.innerText;
      editContact.phone =   this._phoneEl.innerText;
      updContacts[this.state.userPopup.idContact] = editContact;
    }



    this.setState({userPopup:old, contacts: updContacts})
  }

  openProfileHandler = (id) => {
    const oldPopup = {...this.state.userPopup}
    oldPopup.show = true;
    oldPopup.idContact = id;
    this.setState({userPopup: oldPopup})
  }

  render() {
    const itemHead = this.state.tableHead.list.map((item,i) => {
      return (
        <TableCell key={i} type="th" sort={item.sort}>
          <div className="th-sort">
            <div className="th-sort__name">{item.name}</div>

            <Sort
              sort={item.sort}
              sortAsc={() => this.sortHandler(i,"asc")}
              sortDesc={()=> this.sortHandler(i,"desc")}
            />
          </div>
        </TableCell>
      )
    });
    const itemBody = this.state.contacts.map(item => {
      return (
        <TableRow key={item.id} hide={!item.sortShow}>
          <>
            <TableCell type="td">{item.name}</TableCell>
            <TableCell type="td"><div className="player-name" onClick={(ev) => this.openProfileHandler(item.id)}>{item.username}</div></TableCell>
            <TableCell type="td">{item.email}</TableCell>
          </>
        </TableRow>
      )
    });

    return (
      <div className="App" >
        <PopupUser
          onClose={this.closePopupUser}
          onEdit={this.editPopupUser}
          user={this.state.contacts[this.state.userPopup.idContact]}
          show={this.state.userPopup.show}
          edit={this.state.userPopup.edit}
          nameRef={(node) => { this._nameEl = node}}
          emailRef={(node) => { this._emailEl = node}}
          websiteRef={(node) => { this._websiteEl = node}}
          phoneRef={(node) => { this._phoneEl = node}}
        />

        <div className="table-cnl">
          <label className="table-cnl__label">
            Имя
            <input
              onChange={this.changeNameHandler}
              value={this.state.userName}
              className="table-cnl__name"
              type="text"/>
          </label>
        </div>
       <Table>
         <>
          <TableHead>
            <TableRow>
              <>
              {itemHead}
              </>
            </TableRow>
          </TableHead>
           <TableBody>
             <>
               {itemBody}
             </>
           </TableBody>
         </>
       </Table>
      </div>
    );
  }
}

export default App;

import React from 'react';
import PropTypes from 'prop-types';

const tableHead = props => {
  const children = props.children;
  return (
    <thead>
      {children}
    </thead>
  );
};

tableHead.propTypes = {
  children: PropTypes.element.isRequired
};

export default tableHead;

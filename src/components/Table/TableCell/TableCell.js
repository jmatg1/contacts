import React from 'react';
import PropTypes from 'prop-types';

const tableCell = props => {
  const children = props.children
  if (props.type === "td")
    return (
      <td>
        {children}
      </td>
    );

  if (props.type === "th")
    return (
      <th>
        {children}
      </th>
    );
  console.log("ERROR component tableCell: Choose between th or tr");

};

tableCell.propTypes = {
  type: PropTypes.string.isRequired,
};

export default tableCell;

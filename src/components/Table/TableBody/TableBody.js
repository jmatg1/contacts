import React from 'react';
import PropTypes from 'prop-types';

const tableBody = props => {
  const children = props.children;
  return (
    <tbody>
      {children}
    </tbody>
  );
};

tableBody.propTypes = {
  children: PropTypes.element.isRequired,
};

export default tableBody;

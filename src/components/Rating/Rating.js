import React from 'react';
import './Rating.less';
import star from './assets/icons/star.png';
import PropTypes from 'prop-types';


const rating = (props) => {
  let stars = [];
  const style = ["rating-img", props.size]
  for (let i = 1; i <= props.total; i++){
    stars.push(
      <img key={i} className={style.join(" ")} src={star} alt="star"/>
    )
  }

  return (
    <div className="rating-block">
      {stars}
    </div>
  );
};

rating.propTypes = {
  size: PropTypes.string.isRequired,
};

export default rating;

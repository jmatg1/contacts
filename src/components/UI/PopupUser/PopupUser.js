import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './PopupUser.less'
import Button from '../Button/Button'
class PopupUser extends Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="backdrop">
        <div className="modal" >
          {this.props.children}
          <div className="modal__body">
            <div className="modal__body__row">
              <div className="modal__body__row__clm">
                <div className="modal__body__avatar">
                  <img src={this.props.user.avatar} alt=""/>
                </div>
              </div>
              <div className="modal__body__row__clm">
                <div className="modal__body__name" title={this.props.user.name} >
                  Name: <span ref={this.props.nameRef}>{this.props.user.name}</span>
                </div>
                <div className="modal__body__email" title={this.props.user.email}  >
                  Email: <span ref={this.props.emailRef}>{this.props.user.email}</span>
                </div>
                <div className="modal__body__website" title={this.props.user.website}>
                  Website: <span ref={this.props.websiteRef}>{this.props.user.website}</span>
                </div>
                <div className="modal__body__phone" title={this.props.user.phone}>
                  Phone: <span ref={this.props.phoneRef}>{this.props.user.phone}</span>
                </div>
              </div>

            </div>
          </div>
          <div className="modal__footer">
            <Button clicked={this.props.onEdit}>
              {this.props.edit ? "Сохранить" : "Редактировать"}
            </Button>
            <Button clicked={this.props.onClose}>
              Закрыть
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

PopupUser.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default PopupUser;
import React from 'react';
import PropTypes from 'prop-types';
import './Button.less'

const button = props => {
  const children = props.children
  return (
    <button onClick={props.clicked} className="btn">
      {children}
    </button>
  );
};

button.propTypes = {
  children: PropTypes.string.isRequired
};

export default button;
